<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Foodhome;

class listController extends Controller {

    
    public function index(Request $request) {
        
        $listData = [];
        $listFoodhome = [];
        // Get all list foodhome / cafe
        
        if($request->key) {
            $listFoodhome = Foodhome::where('name', $request->key)
            ->orWhere('name', 'like', '%' . $request->key . '%')->get();
        }else {
       $listFoodhome = Foodhome::all();
        }
        if (Auth::check()) {
            $listData = ['name' => Auth::user()->name , 'listFoodhome' => $listFoodhome];
        }else{
            $listData = ['listFoodhome' => $listFoodhome];
        }
        return view('list', $listData);
    }

}
