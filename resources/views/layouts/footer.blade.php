@section('footer')
  <!-- Footer -->
<!---- Javascript ------>
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.2.2/js/swiper.js"></script>
<!---- Javascript ------>

<script>
var swiper = new Swiper('.swiper-container', {
      pagination: {
        el: '.swiper-pagination',
        dynamicBullets: true,
      },
      autoplay: {
            delay: 1500,
        },
    });

</script>
<footer class="container-fluid text-center bg-blue-dark" style="margin-top: 10vh">

       <div class="container bg-light p-4 text-center text-md-left">

      <!-- Grid row -->
      <div class="row">

        <!-- Grid column -->
        <div class="col-md-6 mt-md-0 mt-3">

          <!-- Content -->
          <h5 class="text-uppercase">Quliner</h5>
          <p>Pusat informasi makan sidoarjo yang murah dan berkualitas. Cari informasi segala kuliner disini</p>

        </div>
        <!-- Grid column -->

        <hr class="clearfix w-100 d-md-none pb-3">

        <!-- Grid column -->
        <div class="col-md-3 mb-md-0 mb-3">

            <!-- Links -->
            <h5 class="text-uppercase">Information</h5>

            <ul class="list-unstyled">
              <li>
                <a href="#!">Contact</a>
              </li>
              <li>
                <a href="#!">Email</a>
              </li>
              <li>
                <a href="#!">Phone</a>
              </li>
              <li>
                <a href="#!">Address</a>
              </li>
            </ul>

          </div>
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-md-3 mb-md-0 mb-3">

            <!-- Links -->
            <h5 class="text-uppercase">Law</h5>

            <ul class="list-unstyled">
              <li>
                <a href="#!">Disclaimer</a>
              </li>
              <li>
                <a href="#!">FAQ</a>
              </li>
              <li>
                <a href="#!">Backlink</a>
              </li>
              <li>
                <a href="#!">Link</a>
              </li>
            </ul>

          </div>
          <!-- Grid column -->

      </div>
      <!-- Grid row -->

</footer>
  <div class="copyrights bg-primary text-center text-white p-2 container">
            <p>PT Quliner Media Info © 2018, All Rights Reserved
                <br>
        </div>
    </div>
@endsection