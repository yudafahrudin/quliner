@extends('layouts.admins.app')
@include('layouts.admins.breadcumb')

@section('content')
<div id="userListController" class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    
    <div class="row">
        {{ Breadcrumbs::render('user') }}
        <br>
    </div>
    <div class="row" style="margin-left: 8px">
         <div class="col-md-12" style="background-color: #fff;">
          <h1 class="page-header">Dashboard</h1>

          <div class="row placeholders">
            <div class="col-xs- col-sm-4 placeholder">
                <img style="border-radius: 30%" src="https://cdn4.iconfinder.com/data/icons/small-n-flat/24/user-alt-512.png" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
              <h4>User</h4>
              <span class="text-muted">Total user : 10</span>
            </div>
            <div class="col-xs-6 col-sm-4 placeholder">
              <img style="border-radius: 30%" src="https://cdn4.iconfinder.com/data/icons/ios-web-user-interface-multi-circle-flat-vol-2/512/Attribute_category_label_shop_price_price_tag_tag-512.png" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
              <h4>Category</h4>
              <span class="text-muted">Total category: 4</span>
            </div>
            <div class="col-xs-6 col-sm-4 placeholder">
              <img style="border-radius: 30%" src="https://cdn0.iconfinder.com/data/icons/kameleon-free-pack-rounded/110/Food-Dome-512.png" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
              <h4>Rumah Makan</h4>
              <span class="text-muted">Total rumah makan: 4</span>
            </div>
          </div>

          <h2 class="sub-header">Section title</h2>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Header</th>
                  <th>Header</th>
                  <th>Header</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1,001</td>
                  <td>Lorem</td>
                  <td>ipsum</td>
                  <td>dolor</td>
                </tr>
                <tr>
                  <td>1,002</td>
                  <td>amet</td>
                  <td>consectetur</td>
                  <td>adipiscing</td>
                </tr>
                <tr>
                  <td>1,003</td>
                  <td>Integer</td>
                  <td>nec</td>
                  <td>odio</td>
                </tr>
                <tr>
                  <td>1,003</td>
                  <td>libero</td>
                  <td>Sed</td>
                  <td>cursus</td>
                </tr>
                <tr>
                  <td>1,004</td>
                  <td>dapibus</td>
                  <td>diam</td>
                  <td>Sed</td>
                </tr>
                <tr>
                  <td>1,005</td>
                  <td>Nulla</td>
                  <td>quis</td>
                  <td>sem</td>
                </tr>
                <tr>
                  <td>1,006</td>
                  <td>nibh</td>
                  <td>elementum</td>
                  <td>imperdiet</td>
                </tr>
                <tr>
                  <td>1,007</td>
                  <td>sagittis</td>
                  <td>ipsum</td>
                  <td>Praesent</td>
                  <td>mauris</td>
                </tr>
                <tr>
                  <td>1,008</td>
                  <td>Fusce</td>
                  <td>nec</td>
                  <td>tellus</td>
                </tr>
                <tr>
                  <td>1,009</td>
                  <td>augue</td>
                  <td>semper</td>
                  <td>porta</td>
                </tr>
                <tr>
                  <td>1,010</td>
                  <td>massa</td>
                  <td>Vestibulum</td>
                  <td>lacinia</td>
                </tr>
              </tbody>
            </table>
          </div>
         </div>
     </div>
</div>
@endsection

