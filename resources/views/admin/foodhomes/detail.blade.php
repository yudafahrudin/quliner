<div class="col-sm-12 col-lg-12 main">
    <div class="row">
        <div class="col-md-12">

            @if(session()->has('message'))
            <div class="alert bg-success">
                {{ session()->get('message') }}
                <a href="#" class="pull-right"><em class="fa fa-lg fa-close"></em></a>
            </div>
            @endif

            <div class="panel panel-default">
                <div class="panel-body tabs">
                    <div class="canvas-wrapper">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="tab active">
                                <a href="#profile" data-toggle="tab" aria-expanded="false"> <span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Detail</span> </a>
                            </li>
                            <li class="tab">
                                <a href="#settings" data-toggle="tab" aria-expanded="false"> <span class="visible-xs"><i class="fa fa-cog"></i></span> <span class="hidden-xs">Settings</span> </a>
                            </li>
                            <li class="tab">
                                <a href="#nonactive" data-toggle="tab" aria-expanded="false"> <span class="visible-xs"><i class="fa fa-cog"></i></span> <span class="hidden-xs">Delete</span> </a>
                            </li>
                        </ul>   
                        <div class="tab-content">
                            <div id="profile" class="row tab-pane active">
                                <div class="col-md-3">
                                    <div class="user-bg">
                                        <div class="user-content">
                                            @unless (!Auth::guard('admin')->user())
                                            <img style="width: 100%" src="{{asset('storage/'.$foodhome->images[0]->url)}}" class="img-responsive" alt="">
                                            @endunless
                                        </div>
                                    </div>
                                </div>
                                <div class="white-box col-md-9">
                                    <div class="tab-pane active" id="profile" style="font-size:16px">
                                        <h3><strong>Name :</strong></h3>
                                        {{$foodhome->name}}
                                        <h3><strong>Address :</strong></h3>
                                        {{$foodhome->address}}
                                    </div>
                                </div>

                            </div>
                            <div id="settings" class="row tab-pane">
                                <div class="col-md-3">
                                    <div class="user-bg">
                                        <div class="user-content">
                                            @unless (!Auth::guard('admin')->user())
                                            <img style="width: 100%" src="{{asset('storage/'.$foodhome->images[0]->url)}}" class="img-responsive" alt="">
                                            @endunless
                                        </div>
                                    </div>
                                </div>
                                <div class="white-box col-md-9">
                                    <div class="tab-pane active" id="settings">

                                        <form id="submitUpdate" method="POST" action="{{ route('admin.foodhome.edit',['id'=>object_get($foodhome, 'id')]) }}">
                                            @csrf
                                            <div class="form-group">
                                                <label class="col-md-12">Name</label>
                                                <div class="col-md-12">
                                                    <input type="text" id="name" name="name" placeholder="{{$foodhome->name}}" class="form-control form-control-line" value="{{$foodhome->name}}" required> </div>
                                            </div>
                                            <br />
                                            <div class="form-group">
                                                <label class="col-md-12">Address</label>
                                                <div class="col-md-12">
                                                    <input type="text" id="address" name="address" placeholder="{{$foodhome->address}}" value="{{$foodhome->address}}" class="form-control form-control-line" > </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-12">Price Range</label>
                                                <div class="col-md-12">
                                                    <input type="text" id="address" name="range" placeholder="{{$foodhome->range}}" value="{{$foodhome->range}}" class="form-control form-control-line" > </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-12">map</label>
                                                <div class="col-md-6">
                                                    <input type="text" id="address" name="map_x" placeholder="{{$foodhome->map_x}}" value="{{$foodhome->map_x}}" class="form-control form-control-line" > 
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" id="address" name="map_y" placeholder="{{$foodhome->map_y}}" value="{{$foodhome->map_y}}" class="form-control form-control-line" > 
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-12">Category</label>
                                                <div class="col-md-12">
                                                    <select name="categorie_id" class="form-control form-control-line">
                                                        @foreach ($category as $data)
                                                        <option value="{{$data['id']}}">{{$data['name']}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-12">Description</label>
                                                <div class="col-md-12">
                                                    <textarea type="text" id="address" name="description" class="form-control form-control-line" >{{$foodhome->description}}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <br>
                                                    <button type="submit" id="submitUpdateInput" class="btn btn-primary">
                                                        Update data
                                                    </button>
                                                </div>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>
                            <div id="nonactive" class="row tab-pane">
                                <div class="white-box col-md-12">
                                    <div class="tab-pane active" id="settings">

                                        <div class="form-group">
                                            <label class="col-md-12">Do you want to delete this foodhome ?
                                            </label>
                                            <div class="col-sm-12">
                                                <form action="{{route('admin.foodhome.delete',['id' => $foodhome->id])}}" method="post" class="form-horizontal">
                                                    @csrf
                                                    <input type="submit" id="submitUpdateInput" class="btn btn-danger" value="Delete">
                                                </form>
                                            </div>
                                        </div>
                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {

        var $imageupload = $('.imageupload');
        $imageupload.imageupload({
            allowedFormats: ["jpg", "jpeg", "png", "gif"],
            maxWidth: 200,
            maxHeight: 250,
            maxFileSizeKb: 2048
        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $("#submitUpdate").on('submit', (function (e) {

            const url = e.currentTarget.action
            const data = new FormData(this);
            const type = "POST";
            const contentType = false; // The content type used when sending data to the server.
            const cache = false; // To unable request pages to be cached
            const processData = false;

            e.preventDefault();
            $.confirm({
                title: 'Are you sure ?',
                content: 'Update this user  ',
                buttons: {
                    confirm: function (e) {
                        $.ajax({
                            url,
                            data,
                            type,
                            contentType,
                            cache,
                            processData,
                            success: function (data) {
                                notifyMessage(data.status, data.message);
                                $('.modal-body').load('{{url("admin/foodhomes/detail")}}/{{$foodhome->id}}');

                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                notifyMessage('error', thrownError);
                            }

                        });
                    },
                    cancel: function () {
                    },
                },
                closeIcon: true,
                animation: 'scale',
            });
        }));
    });
</script>
